import React from 'react';
import './App.css';
import data from './data/data.js';
import Item from './components/Item.js';
import toolbar from './components/Toolbar.js';
import Toolbar from './components/Toolbar.js';
/*import Sound from 'react-native-sound';*/
/*import useSound from 'use-sound';*/

class App extends React.Component {

  constructor(props) {
    super();
    this.filterfunctie = this.handleFilterChange.bind(this);
    this.filterfunctie1 = this.filterfunctie1.bind(this);

    this.state = {
      filterwaarde: '',
      dashboardwaarde: 0
    }
  }

  filterfunctie1(filter) {
    console.log('filter dieren met: ' + filter);
    data[0].items.filter(item => item.name.toLocaleLowerCase().includes(filter.toLocaleLowerCase())).forEach(item => console.log(item));
    console.log('filter dieren met: '+this.state.filterwaarde);
    data[0].items.filter(item => item.name.toLocaleLowerCase().includes(this.state.filterwaarde.toLocaleLowerCase())).forEach(item => console.log(item));
    
  }

  handleFilterChange(filter) {
    this.setState({ filterwaarde: filter });


    /*filterfunctie1(filter); bestaat zogezegd niet? Ah gebruik this!*/
    //this.filterfunctie1(filter);
  }
  handleDashChange = (dashboardwaarde) =>{
    this.setState({ dashboardwaarde });
    console.log(dashboardwaarde);
  }



  render() {

    // werkt niet beter, nog altijd rare filtering
    //const filtLijst = data[0].items.filter(item => item.name.toLocaleLowerCase().includes(this.state.filterwaarde.toLocaleLowerCase()));

    return (
      <div className="App">
        <header>
          <h1>Soundboard</h1>
        </header>
        <div className="toolbar">
          <Toolbar
            filterDoorgeven={this.filterfunctie}
            filterwaarde={this.state.filterwaarde}
            handleDashChange={this.handleDashChange}
            dashboardwaarde={this.state.dashboardwaarde}
          />
        </div>
        <div className="soundboard">
          {data[this.state.dashboardwaarde].items
            .filter(item => 
              item.name.toLocaleLowerCase().includes(this.state.filterwaarde.toLocaleLowerCase()))
            .map((item) => 
               //niet .map((item, index)
               //key={index}> key is not defined..
              <Item item={item} key={item.name} />
            )
            //
          /* werkt wel, maar afbeeldingen veranderen gewoon niet direct? het is wat raar..
          Ah Warning: Each child in a list should have a unique "key" prop.
          Check the render method of `App`. See https://fb.me/react-warning-keys for more information.
              in Item (at App.js:61)
              in App (at src/index.js:9)
              in StrictMode (at src/index.js:8)
              
              
              */

              
            }

        </div>
        <footer>

        </footer>
      </div>
    )
  }

};



export default App;

/* format code: shift + alt + f

       {data[0].items.map(item => <Item />
          )}

             <div  className="soundboard">
         {data[0].items.map( item =>
         <div  className="item" onClick={() => playAudio(item.sound)}>
         <div className="name">{item.name}</div>
         <img src={item.img} alt={item.name}/>
          </div>)}
         </div>


   <div  className="item">
             <div className="name">{data[0].items[0].name}</div>
             <img src={data[0].items[0].img} alt={data[0].items[0].name} />
           </div>
           <div  className="item">
             <div className="name">{data[0].items[1].name}</div>
             <img src={data[0].items[1].img} alt={data[0].items[1].name} />
           </div>
           <div  className="item">
             <div className="name">{data[0].items[2].name}</div>
             <img src={data[0].items[2].img} alt={data[0].items[2].name} />
           </div>

      <div >
         <h1>Soundboard</h1>
         <div className="FlexInput">
         <select name="cars" id="cars">
     <option value="volvo">Dieren</option>
     <option value="saab">Omgeving</option>
   </select>
         <input id="zoekInput" type="text"></input>
         </div>
         <div className="Flex">
         <img src="img/horse2.png" alt="horse" />
         <img src="img/chicken.png" alt="chicken" />
         <img src="img/pig.png" alt="pig" />
         <img src="img/mouse.png" alt="mouse" />

         </div>
       </div>

       <div className="App">
         <header className="App-header">
           <img src={logo} className="App-logo" alt="logo" />
           <p>
             Edit <code>src/App.js</code> and save to reload.
           </p>
           <a
             className="App-link"
             href="https://reactjs.org"
             target="_blank"
             rel="noopener noreferrer"
           >
             Learn React
           </a>
         </header>
       </div>
       */