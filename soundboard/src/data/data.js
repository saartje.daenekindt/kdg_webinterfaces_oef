import kraaiSound from "../assets/sounds/kraai.wav";
import kipSound from "../assets/sounds/haan.wav";
import mugSound from "../assets/sounds/mug.wav";
import olifSound from "../assets/sounds/olifant.wav";
import paardSound from "../assets/sounds/paard.wav";
import uilSound from "../assets/sounds/uil.wav";
import varkenSound from "../assets/sounds/varken.wav";
import ocean from "../assets/sounds/Ocean.wav";
import forest from "../assets/sounds/Forest.wav";
import snow from "../assets/sounds/snow.wav";

const data = [ {
    name: "Dieren",
    items : [{
        name: "Paard",
        img: "https://cdn0.iconfinder.com/data/icons/kameleon-free-pack-rounded/110/Wooden-Horse-512.png",
        sound: paardSound,
    },
    {
        name: "Haan",
        img: "https://image.flaticon.com/icons/png/512/424/424808.png",
        sound: kipSound,
    },
    {
        name: "Mug",
        img: "https://cdn0.iconfinder.com/data/icons/insects-color/512/mosquito-256.png",
        sound: mugSound,
    },
    {
        name: "Uil",
        img: "https://image.flaticon.com/icons/png/512/185/185811.png",
        sound: uilSound,
        
    },
    {
        name: "Varken",
        img: "https://image.flaticon.com/icons/png/512/185/185827.png",
        sound: varkenSound,
        
    },
    {
        name: "Olifant",
        img: "https://image.flaticon.com/icons/png/512/185/185826.png",
        sound: olifSound,
        
    },
    {
        name: "Kraai",
        img: "https://image.flaticon.com/icons/png/512/272/272817.png",
        sound: kraaiSound,
        
    },
]
},

{   name: "Omgeving",
    items : [{
        name: "Zee",
        img: "https://image.flaticon.com/icons/png/512/191/191038.png",
        sound: ocean,
    },
    {
        name: "Jungle",
        img: "http://images.fineartamerica.com/images/artworkimages/medium/1/tropical-jungle-plants-illustration-jit-lim-transparent.png",
        sound: forest,
    },
    {
        name: "Snow",
        img: "https://www.shareicon.net/data/2016/09/16/829676_snow_512x512.png",
        sound: snow,
    }

]}

]

export default data;