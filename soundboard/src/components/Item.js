import React from 'react';
import '../css/item.css';

class Item extends React.Component{

    constructor(props){
        super();
        this.name = props.item.name;
        this.img = props.item.img;
        this.sound = props.item.sound;
        this.state = {
            selected: false
        }
    }

    /*function playAudio(sound) {
        console.log(sound)
        let audio = new Audio(sound);
        audio.play();
    
      }

      playSound(sound) {
          console.log(sound)
        let audio = new Audio(sound);
        audio.play();
      }

      werkt niet..

      */
    playSound = () => new Audio(this.sound).play();
    
    render (){
      //console.log('dier:'+this.name)
        return (
            <div  className="item" 
            onClick={this.playSound}
            onMouseEnter={() => this.setState({ selected:true})}
            onMouseLeave={() => this.setState({ selected:false})}
            >
          <div className={this.state.selected ? 'showName' : 'name'}>{this.name}</div>
          <img src={this.img} alt={this.name} />
        </div>
        )
    }
};

export default Item;