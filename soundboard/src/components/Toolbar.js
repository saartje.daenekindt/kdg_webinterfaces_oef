import React from 'react';

class Toolbar extends React.Component {
    constructor(props){
        super();
        
       this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e){
        this.props.filterDoorgeven(e.target.value);
    }
    handleNieuwDashboard= (e) => {
        //geen bind nodig
        this.props.handleDashChange(e.target.value);
    }

    render() {
        return (
            <toolbar className='toolb'>
                <select name="kies" id="kies" onChange={this.handleNieuwDashboard}>
                    <option value="0">Dieren</option>
                    <option value="1">Omgeving</option>
                </select>
                <input id="zoekInput" type="text" onChange={this.handleChange}
                value={this.props.filterwaarde}
                ></input>
            </toolbar>

        )
    }
};

export default Toolbar;

/*
<input id="zoekInput" type="text" onChange={(event) => this.setState({ filterwaarde : event.target.value})}
                value={this.state.filterwaarde}

constructor(props){
        super();
    
        this.state = {
            filterwaarde: ''
        }
    }
*/